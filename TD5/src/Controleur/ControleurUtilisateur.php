<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\ModeleUtilisateur;
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('vueGenerale.php', [
            "titre" => "Liste des utilisateurs",
            "cheminCorpsVue" => "utilisateur/liste.php",
            "utilisateurs" => $utilisateurs
        ]);
    }

    public static function afficherDetail() : void {
        $login = $_GET['login'];
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login);
        if ($utilisateur == NULL) {
            ControleurUtilisateur::afficherVue('vueGenerale.php', [
                "titre" => "Erreur",
                "cheminCorpsVue" => "utilisateur/erreur.php"
            ]);
        }
        else {
            ControleurUtilisateur::afficherVue("vueGenerale.php", [
                "titre" => "Détails utilisateur",
                "cheminCorpsVue" => 'utilisateur/detail.php',
                'utilisateur' => $utilisateur
            ]);
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation() : void
    {
        ControleurUtilisateur::afficherVue("vueGenerale.php", [
            "titre" => "Ajout utilisateur",
            "cheminCorpsVue" => 'utilisateur/formulaireCreation.php'
        ]);
    }

    public static function creerDepuisFormulaire() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);
        $utilisateur->ajouter();
        ControleurUtilisateur::afficherVue("vueGenerale.php", [
            "titre" => "utilsiateur cree",
            "cheminCorpsVue" => "utilisateur/utilisateurCree.php",
            "utilisateurs" => $utilisateurs
        ]);
    }
}
?>
