<!DOCTYPE html>
<html>

<body>
<?php
/** @var ModeleUtilisateur[] $utilisateurs */

use App\Covoiturage\Modele\ModeleUtilisateur;

foreach ($utilisateurs as $utilisateur){
    $loginHTML = $utilisateur->getLogin();
    $loginURL = $utilisateur->getLogin();
    echo '<p> Utilisateur de login ' . htmlspecialchars($loginHTML) . ' <a href="controleurFrontal.php?action=afficherDetail&login=' . rawurlencode($loginURL) . '">voir plus d information</a>' . '.</p>';
}
echo '<p> <a href="controleurFrontal.php?action=afficherFormulaireCreation">Créer un utilisateur</a>';
?>
</body>
</html>
