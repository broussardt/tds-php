<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class ControleurTrajet
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        ControleurTrajet::afficherVue('vueGenerale.php', [
            "titre" => "Liste des trajets",
            "cheminCorpsVue" => "trajet/liste.php",
            "trajets" => $trajets
        ]);
    }

    public static function afficherDetail(): void
    {
        $id = $_GET['id'];
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        if ($trajet == NULL) {
            ControleurTrajet::afficherErreur($id);
        } else {
            ControleurTrajet::afficherVue("vueGenerale.php", [
                "titre" => "Détails trajet",
                "cheminCorpsVue" => 'trajet/detail.php',
                'trajet' => $trajet
            ]);
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation(): void
    {
        ControleurTrajet::afficherVue("vueGenerale.php", [
            "titre" => "Ajout trajet",
            "cheminCorpsVue" => 'trajet/formulaireCreation.php'
        ]);
    }

    public static function creerDepuisFormulaire(): void
    {
        $trajets = (new TrajetRepository())->recuperer();
        $depart = $_GET['depart'];
        $arrivee = $_GET['arrivee'];
        $date = new DateTime($_GET['date']);
        $prix = $_GET['prix'];
        $loginConducteur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['conducteurLogin']);
        $nonFumeur = isset($_GET['nonFumeur']);
        $trajet = new Trajet(null, $depart, $arrivee, $date, $prix, $loginConducteur, $nonFumeur, array());
        (new TrajetRepository())->ajouter($trajet);
        ControleurTrajet::afficherVue("vueGenerale.php", [
            "titre" => "trajet cree",
            "cheminCorpsVue" => "trajet/trajetCree.php",
            "trajets" => $trajets
        ]);
    }

    public static function afficherErreur(string $messageErreur): void
    {
        ControleurTrajet::afficherVue("vueGenerale.php", [
            "messageErreur" => $messageErreur,
            "cheminCorpsVue" => "trajet/erreur.php"
        ]);
    }

    public static function afficherSupressionTrajet(): void
    {
        $trajets = (new TrajetRepository())->recuperer();
        (new TrajetRepository())->supprimer($_GET['id']);
        ControleurTrajet::afficherVue("vueGenerale.php", [
            "titre" => "Trajet supprimé",
            "cheminCorpsVue" => "trajet/trajetSupprime.php",
            "id" => $_GET['id'],
            "trajets" => $trajets
        ]);
    }
}/**
 * public static function afficherFormulaireMiseAJour(): void
 * {
 * $trajet = TrajetRepository::recupererTrajetParLogin($_GET['login']);
 * ControleurTrajet::afficherVue("vueGenerale.php", [
 * "titre" => "Trajet mis à jour",
 * "cheminCorpsVue" => "trajet/formulaireMiseAJour.php",
 * "trajet" => $trajet
 * ]);
 * }
 *
 * public static function mettreAJour(): void
 * {
 * $login = $_GET['login'];
 * $nom = $_GET['nom'];
 * $prenom = $_GET['prenom'];
 *
 * $trajet = new Trajet($login, $nom, $prenom);
 * $trajets = TrajetRepository::recupererTrajets();
 * TrajetRepository::mettreAJour($trajet);
 * ControleurTrajet::afficherVue("vueGenerale.php", [
 * 'login' => $login,
 * 'trajet' => $trajet,
 * 'titre' => 'Mis à jour',
 * 'cheminCorpsVue' => 'trajet/trajetMisAJour.php',
 * 'trajets' => $trajets
 * ]);
 * }
 * }
 */
