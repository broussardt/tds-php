<!DOCTYPE html>
<html>

<body>
<?php
use App\Covoiturage\Modele\DataObject\Trajet;

/** @var Trajet[] $trajets */
foreach ($trajets as $trajet){
    $idHTML = htmlspecialchars($trajet->getId());
    $idURL = rawurlencode($trajet->getId());
    echo "<p> Trajet d'id " . $idHTML . ' <a href="controleurFrontal.php?controleur=trajet&action=afficherDetail&id=' . $idURL . '">(voir plus d information)</a>' . '<a href="controleurFrontal.php?controleur=trajet&action=afficherSupressionTrajet&id=' . $idURL . '"> (supprimerTrajet)</a>' . '<a href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireMiseAJour&id=' . $idURL . '"> (mettre à jour)</a>' . '.</p>';
}
echo '<p> <a href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireCreation">Créer un trajet</a>';
?>
</body>
</html>
