<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> Mon premier php </title>
</head>
<body>
<div>
    <form method="get" action="controleurFrontal.php">
        <input type="hidden" name="controleur" value="trajet">
        <input type="hidden" name="action" value="creerDepuisFormulaire">
        <fieldset>
            <legend>Mon formulaire :</legend>
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="depart_id">Depart&#42;</label>
                <input class="InputAddOn-field" type="text" placeholder="Montpellier" name="depart" id="depart_id" required/>
            </p>
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="arrivee_id">Arrivée&#42;</label>
                <input class="InputAddOn-field" type="text" placeholder="Sète" name="arrivee" id="arrivee_id" required/>
            </p>
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="date_id">Date&#42;</label>
                <input class="InputAddOn-field" type="date" placeholder="JJ/MM/AAAA" name="date" id="date_id"  required/>
            </p>
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="prix_id">Prix&#42;</label>
                <input class="InputAddOn-field" type="number" placeholder="20" name="prix" id="prix_id"  required/>
            </p>
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="conducteurLogin_id">Login du conducteur&#42;</label>
                <input class="InputAddOn-field" type="text" placeholder="leblancj" name="conducteurLogin" id="conducteurLogin_id" required/>
            </p>
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="nonFumeur_id">Non Fumeur ?&#42;</label>
                <input type="hidden" name="nonFumeur" value="0" id="nonFumeur_id">
                <input class="InputAddOn-field" type="checkbox" name="nonFumeur" value="1" id="nonFumeur_id"/>
            </p>
            <p>
                <input type="submit" value="Envoyer" />
            </p>
        </fieldset>
    </form>
</div>
</body>
</html>
