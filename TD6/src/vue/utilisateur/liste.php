<!DOCTYPE html>
<html>

<body>
<?php
/** @var Utilisateur[] $utilisateurs */

use App\Covoiturage\Modele\Utilisateur;

foreach ($utilisateurs as $utilisateur){
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode($utilisateur->getLogin());
    echo '<p> Utilisateur de login ' . $loginHTML . ' <a href="controleurFrontal.php?action=afficherDetail&login=' . $loginURL . '">(voir plus d information)</a>' . '<a href="controleurFrontal.php?action=afficherSupressionUtilisateur&login=' . $loginURL . '"> (supprimerUtilisateur)</a>' . '<a href="controleurFrontal.php?action=afficherFormulaireMiseAJour&login=' . $loginURL . '"> (mettre à jour)</a>' . '.</p>';
}
echo '<p> <a href="controleurFrontal.php?action=afficherFormulaireCreation">Créer un utilisateur</a>';
?>
</body>
</html>
