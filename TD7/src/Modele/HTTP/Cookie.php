<?php
namespace App\Covoiturage\Modele\HTTP;

class Cookie
{
    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void
    {
        if ($dureeExpiration == null) {
            $dureeExpiration = 0;
        }
        setcookie($cle, json_encode($valeur), $dureeExpiration);
    }

    public static function lire(string $cle): mixed
    {
        return json_decode($_COOKIE[$cle]);
    }

    public static function contient($cle) : bool
    {
        return isset($_COOKIE[$cle]);
    }

    public static function supprimer($cle) : void
    {
        unset($_COOKIE[$cle]);
        setcookie($cle, "", 1);
    }
}
