<?php
namespace App\Covoiturage\Modele\Repository;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
abstract class AbstractRepository {
    public function mettreAJour(AbstractDataObject $objet): void
    {
        $sql = "UPDATE " . $this->getNomTable() . " SET nom = :nomTag, prenom = :prenomTag WHERE login = :loginTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "loginTag" => $objet->getLogin(),
            "nomTag" => $objet->getNom(),
            "prenomTag" => $objet->getPrenom()
        );
        $pdoStatement->execute($values);
    }

    public function ajouter(AbstractDataObject $objet): bool
    {
        $sql = "INSERT INTO " . $this->getNomTable() . " (" . join(", " , $this->getNomsColonnes()) . ") VALUES (:" . join("Tag, :", $this->getNomsColonnes()) . "Tag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = $this->formatTableauSQL($objet);
        $pdoStatement->execute($values);
        return true;
    }

    public function supprimer($valeurClePrimaire): void
    {
        $sql = "DELETE FROM ". $this->getNomTable() ." WHERE " . $this->getNomClePrimaire() . " = :primaryTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $pdoStatement->execute(array("primaryTag" => $valeurClePrimaire));
    }

    public function recupererParClePrimaire(string $primary): ?AbstractDataObject
    {
        $sql = 'SELECT * from ' . $this->getNomTable() . ' WHERE '. $this->getNomClePrimaire() .' = :primaryTag';
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "primaryTag" => $primary,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $objetFormatTableau = $pdoStatement->fetch();
        if (!$objetFormatTableau) {
            return null;
        }

        return $this->construireDepuisTableauSQL($objetFormatTableau);
    }

    public function recuperer(): array
    {
        $objets = array();
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query('SELECT * FROM '. $this->getNomTable());
        foreach ($pdoStatement as $objet) {
            $objets[] = $this->construireDepuisTableauSQL($objet);
        }
            return $objets;
    }

    protected abstract function getNomTable(): string;
    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;
    protected abstract function getNomClePrimaire() : string;
    protected abstract function getNomsColonnes():array;

    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;
}
