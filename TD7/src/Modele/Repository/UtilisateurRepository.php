<?php
namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;
class UtilisateurRepository extends AbstractRepository {

    public function construireDepuisTableauSQL(array $utilisateurFormatTableau): Utilisateur
    {
        return new Utilisateur($utilisateurFormatTableau['login'], $utilisateurFormatTableau['nom'], $utilisateurFormatTableau['prenom']);
    }

    public static function recupererTrajetsCommePassager(Utilisateur $utilisateur) : array {
        $sql = "SELECT t.* FROM trajet t JOIN passager p ON t.id = p.trajetId WHERE p.passagerLogin = :utilisateurLoginTag;";
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->prepare($sql);
        $pdoStatement->execute(array("utilisateurLoginTag" => $utilisateur->getLogin()));

        $trajets = [];

        foreach ($pdoStatement as $ligne) {
            $trajet = [
                'id' => $ligne['id'],
                'depart' => $ligne['depart'],
                'arrivee' => $ligne['arrivee'],
                'date' => $ligne['date'],
                'prix' => $ligne['prix'],
                'conducteurLogin' => $ligne['conducteurLogin'],
                'nonFumeur' => $ligne['nonFumeur'],
            ];

            $trajets[] = (new TrajetRepository())->construireDepuisTableauSQL($trajet);
        }

        return $trajets;
    }
    protected function getNomTable(): string
    {
        return "utilisateur";
    }
    protected function getNomClePrimaire(): string
    {
        return "login";
    }
    protected function getNomsColonnes(): array
    {
        return ["login", "nom", "prenom"];
    }
    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        /** @var Utilisateur $utilisateur */
        return array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom()
        );
    }
}
