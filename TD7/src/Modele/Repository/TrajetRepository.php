<?php
namespace App\Covoiturage\Modele\Repository;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;

use DateTime;
class TrajetRepository extends AbstractRepository {
    protected function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]),
            $trajetTableau["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($trajetTableau["conducteurLogin"]),
            $trajetTableau["nonFumeur"],
            array()
        );
        $trajet->setPassagers(TrajetRepository::recupererPassagers($trajet));
        return $trajet;
    }

    static public function recupererPassagers(Trajet $trajet): array {
        $res = array();
        $sql = "SELECT utilisateur.*
                FROM utilisateur 
                INNER JOIN passager ON utilisateur.login = passager.passagerLogin 
                WHERE passager.trajetId = :trajetId";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            'trajetId' => $trajet->getId(),
        );
        $pdoStatement->execute($values);
        foreach ($pdoStatement as $passagerFormatTableau) {
            $res[] = (new UtilisateurRepository())->construireDepuisTableauSQL($passagerFormatTableau);
        }
        return $res;
    }

    protected function getNomTable(): string
    {
        return "trajet";
    }

    protected function getNomClePrimaire(): string
    {
        return "id";
    }
    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"];
    }
    protected function formatTableauSQL(AbstractDataObject $objet): array
    {
        /** @var Trajet $objet */
        return array(
            "idTag" => $objet->getId(),
            "departTag" => $objet->getDepart(),
            "arriveeTag" => $objet->getArrivee(),
            "dateTag" => $objet->getDate()->format("Y/m/d"),
            "prixTag" => $objet->getPrix(),
            "conducteurLoginTag" => $objet->getConducteur()->getLogin(),
            "nonFumeurTag" => $objet->isNonFumeur()
        );
    }


}