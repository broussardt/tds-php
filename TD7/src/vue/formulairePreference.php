<?php
use App\Covoiturage\Modele\HTTP\Cookie;
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form method="get" action="controleurFrontal.php">
    <input type='hidden' name='action' value='enregistrerPreference'>
    <fieldset>
        <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur"
            <?php if (Cookie::contient('preferenceControleur') && Cookie::lire('preferenceControleur') == "utilisateur") {echo "checked";}?>>
        <label for="utilisateurId">Utilisateur</label>
        <input type="radio" id="trajetId" name="controleur_defaut" value="trajet"
            <?php if (Cookie::contient('preferenceControleur') && Cookie::lire('preferenceControleur') == "trajet") {echo "checked";}?>>
        <label for="trajetId">Trajet</label>
        <input type="submit" value="Envoyer" />
    </fieldset>
</form>
</body>
</html>
