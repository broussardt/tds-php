<!DOCTYPE html>
<html>
<body>
<?php
use App\Covoiturage\Modele\DataObject\Utilisateur;
/** @var Utilisateur $utilisateur*/
echo '<p> ' . htmlspecialchars($utilisateur->getNom()) . ' ' . htmlspecialchars($utilisateur->getPrenom()) . ' de login ' . htmlspecialchars($utilisateur->getLogin()) . '.</p>';
?>
</body>
</html>
