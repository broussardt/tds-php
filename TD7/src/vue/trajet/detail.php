<?php
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\DataObject\Trajet;
/** @var DateTime $dateHTML */
/** @var Utilisateur $conducteurHTML */
/** @var Trajet $trajet */

$nonFumeurHTML = htmlspecialchars($trajet->isNonFumeur()) ? " non fumeur" : " ";
$dateHTML = $trajet->getDate();
$departHTML = htmlspecialchars($trajet->getDepart());
$arriveeHTML = htmlspecialchars($trajet->getArrivee());
$conducteurHTML = $trajet->getConducteur();
echo "<p> Le trajet $nonFumeurHTML du {$trajet->getDate()->format("d/m/Y")} partira de {$departHTML} pour aller à {$arriveeHTML} (conducteur: {$conducteurHTML->getPrenom()} {$conducteurHTML->getNom()}).</p>";