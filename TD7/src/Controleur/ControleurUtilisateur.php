<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\HTTP\Cookie;

class ControleurUtilisateur extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('vueGenerale.php', [
            "titre" => "Liste des utilisateurs",
            "cheminCorpsVue" => "utilisateur/liste.php",
            "utilisateurs" => $utilisateurs
        ]);
    }

    public static function afficherDetail() : void {
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if ($utilisateur == NULL) {
            ControleurUtilisateur::afficherErreur($login);
        }
        else {
            ControleurUtilisateur::afficherVue("vueGenerale.php", [
                "titre" => "Détails utilisateur",
                "cheminCorpsVue" => 'utilisateur/detail.php',
                'utilisateur' => $utilisateur
            ]);
        }
    }

    public static function afficherFormulaireCreation() : void
    {
        ControleurUtilisateur::afficherVue("vueGenerale.php", [
            "titre" => "Ajout utilisateur",
            "cheminCorpsVue" => 'utilisateur/formulaireCreation.php'
        ]);
    }

    public static function creerDepuisFormulaire() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        $utilisateur = new Utilisateur($login, $nom, $prenom);
        (new UtilisateurRepository())->ajouter($utilisateur);
        ControleurUtilisateur::afficherVue("vueGenerale.php", [
            "titre" => "utilsiateur cree",
            "cheminCorpsVue" => "utilisateur/utilisateurCree.php",
            "utilisateurs" => $utilisateurs
        ]);
    }

    public static function afficherErreur(string $messageErreur) : void {
        ControleurUtilisateur::afficherVue("vueGenerale.php", [
            "messageErreur" => $messageErreur,
            "cheminCorpsVue" => "utilisateur/erreur.php"
        ]);
    }

    public static function afficherSupressionUtilisateur() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        (new UtilisateurRepository())->supprimer($_GET['login']);
        ControleurUtilisateur::afficherVue("vueGenerale.php", [
            "titre" => "Utilisateur supprimé",
            "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php",
            "login" => $_GET['login'],
            "utilisateurs" => $utilisateurs
        ]);
    }

    public static function afficherFormulaireMiseAJour() : void {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        ControleurUtilisateur::afficherVue("vueGenerale.php", [
            "titre" => "Utilisateur mis à jour",
            "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php",
            "utilisateur" => $utilisateur
        ]);
    }

    public static function mettreAJour() : void {
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];

        $utilisateur = new Utilisateur($login, $nom, $prenom);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        UtilisateurRepository::mettreAJour($utilisateur);
        ControleurUtilisateur::afficherVue("vueGenerale.php", [
            'login' => $login,
            'utilisateur' => $utilisateur,
            'titre' => 'Mis à jour',
            'cheminCorpsVue' => 'utilisateur/utilisateurMisAJour.php',
            'utilisateurs' => $utilisateurs
        ]);
    }

//    public static function testSession() : void {
//        $session = Session::getInstance();
//        $session->enregistrer("nom", "Broussard");
//        $session->enregistrer("age", 19);
//        echo $session->lire("nom");
//        echo $session->lire("age");
//        $session->supprimer("age");
//        $session->detruire();
//    }
}
?>
