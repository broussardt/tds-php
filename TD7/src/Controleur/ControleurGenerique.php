<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique
{
    protected static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulairePreference(): void
    {
        self::afficherVue("vueGenerale.php", [
            "titre" => "choix Controleur",
            "cheminCorpsVue" => "formulairePreference.php",
        ]);
    }

    public static function enregistrerPreference() : void
    {
        $controleur = $_GET["controleur_defaut"];
        echo "test";
        echo $controleur;
        PreferenceControleur::enregistrer($controleur);
        self::afficherVue("vueGenerale.php", [
            "titre" => "enregistrement preference",
            "cheminCorpsVue" => "preferenceEnregistre.php",
        ]);
    }
}