<?php
use App\Covoiturage\Controleur\ControleurUtilisateur;
use App\Covoiturage\Lib\Psr4AutoloaderClass;

require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';
// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');
// On récupère l'action passée dans l'URL
if (isset($_GET['action'])) {
    $action = $_GET['action'];
}
else {
    $action = "afficherListe";
}

$controleur = "utilisateur";
if (\App\Covoiturage\Modele\HTTP\Cookie::contient('preferenceControleur')) {
    $controleur = \App\Covoiturage\Modele\HTTP\Cookie::lire('preferenceControleur');
}
if (isset($_GET['controleur'])) {
    $controleur = $_GET['controleur'];
}

$nomDeClasseControleur = 'App\Covoiturage\Controleur\Controleur' . ucfirst($controleur);
if (class_exists($nomDeClasseControleur)) {
    if (in_array($action, get_class_methods($nomDeClasseControleur))) {
        // Appel de la méthode statique $action de ControleurUtilisateur
        $nomDeClasseControleur::$action();
    }
    else {
        ControleurUtilisateur::afficherErreur("Ce controleur n'existe pas");
    }
}
?>
