<?php

require_once 'Modele/ConnexionBaseDeDonnees.php';
require_once 'Modele/ModeleUtilisateur.php';

class Trajet {






    public function __toString()
    {
        $nonFumeur = $this->nonFumeur ? " non fumeur" : " ";
        return "<p>
            Le trajet$nonFumeur du {$this->date->format("d/m/Y")} partira de {$this->depart} pour aller à {$this->arrivee} (conducteur: {$this->conducteur->getPrenom()} {$this->conducteur->getNom()}).
        </p>";
    }

    /**
     * @return Trajet[]
     */


    public function ajouter() : void
    {
        $requete = "INSERT INTO trajet (depart, arrivee, date, prix, conducteurLogin, nonFumeur) VALUES (:depart, :arrivee, :date, :prix, :conducteurLogin, :nonFumeur)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($requete);

        $pdoStatement->execute([
            'depart' => $this->depart,
            'arrivee' => $this->arrivee,
            'date' => $this->date->format("Y/m/d"),
            'prix' => $this->prix,
            'conducteurLogin' => $this->conducteur->getLogin(),
            'nonFumeur' => $this->nonFumeur,
        ]);
    }

    /**
     * @return ModeleUtilisateur[]
     */

}
