<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP :
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /*Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */

          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
          /*$texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
          /*echo $texte;
          $nom = "Broussard";
          $prenom = "Tim";
          $login = "broussardt";
          echo "Utilisateur $nom $prenom de login $login";*/
          $utilisateur = [
                  'nom' => 'Broussard',
              'prenom' => 'Tim',
              'login' => 'broussardt'
          ];
          /*print_r($utilisateur);
          echo "Utilisateur $utilisateur[nom] $utilisateur[prenom] de login $utilisateur[login]";*/

        $utilisateurs = [
                "a",
                "b",
                "c"
          ];
        foreach ($utilisateurs as $utilisateur) {
            echo "<ul><li>$utilisateur</li></ul>";
        }
        if (count($utilisateurs) == 0) {
            echo "Aucun utilisateur";
        }
        ?>
    </body>
</html> 