<?php
require_once 'ConnexionBaseDeDonnees.php';
require_once 'Trajet.php';
class Utilisateur
{

    private string $login;
    private string $nom;
    private string $prenom;
    private ?array $trajetsCommePassager;

    // un getter
    public function getNom() : string
    {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom,
   ) {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
    }

    /**
     * @return mixed
     */
    public function getLogin() : string
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin(string $login): void
    {
        $this->login = substr($login, 0, 64);;
    }

    /**
     * @return mixed
     */
    public function getPrenom() : string
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public function getTrajetsCommePassager(): ?array
    {
        if (is_null($this->trajetsCommePassager)) {
            $this->trajetsCommePassager = $this->recupererTrajetsCommePassager();
        }
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): Utilisateur
    {
        return new Utilisateur($utilisateurFormatTableau['login'], $utilisateurFormatTableau['nom'], $utilisateurFormatTableau['prenom']);
    }

    public static function recupererUtilisateurs(): array {
        $utilisateurs  = array();
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query('SELECT * FROM utilisateur');
        foreach ($pdoStatement as $utilisateur) {
            $utilisateurs[] = Utilisateur::construireDepuisTableauSQL($utilisateur);
        }
        return $utilisateurs;
    }

    public static function recupererUtilisateurParLogin(string $login) : ?Utilisateur {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();
        if (!$utilisateurFormatTableau) {
            return null;
        }

        return Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter() : void {
        $sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES (:login, :nom, :prenom)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "login" => $this->login,
            "nom" => $this->nom,
            "prenom" => $this->prenom
        );
        $pdoStatement->execute($values);
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString() : string
    {
        return "Utilisateur $this->nom $this->prenom de login $this->login";
    }

    /**
     * @return Trajet[]
     */
    /**
     * @return Trajet[]
     */
    private function recupererTrajetsCommePassager() : array {
        $sql = "SELECT t.* FROM trajet t JOIN passager p ON t.id = p.trajetId WHERE p.passagerLogin = :utilisateurLoginTag;";
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->prepare($sql);
        $pdoStatement->execute(array("utilisateurLoginTag" => $this->login));

        $trajets = [];

        foreach ($pdoStatement as $ligne) {
            $trajet = [
                'id' => $ligne['id'],
                'depart' => $ligne['depart'],
                'arrivee' => $ligne['arrivee'],
                'date' => $ligne['date'],
                'prix' => $ligne['prix'],
                'conducteurLogin' => $ligne['conducteurLogin'],
                'nonFumeur' => $ligne['nonFumeur'],
            ];

            $trajets[] = Trajet::construireDepuisTableauSQL($trajet);
        }

        return $trajets;
    }
}



